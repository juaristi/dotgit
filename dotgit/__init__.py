import sys
import dotgit.cmds

def run_dotgit_init():
    """ dg init <repo URL> """
    if len(sys.argv) != 3:
        print("Usage: dg init <repo URL>")
        return 1

    url = sys.argv[2]

    initializer = dotgit.cmds.Initializer()
    initializer.run(url)
    return 0

def run_dotgit_scatter():
    """ dg scatter """
    scatterer = dotgit.cmds.Scatterer()
    scatterer.run()
    return 0

def run_dotgit_gather():
    """ dg gather """
    gatherer = dotgit.cmds.Gatherer()
    gatherer.run()
    return 0

def run_dotgit():
    if len(sys.argv) <= 1:
        print("Usage: %s init|scatter|gather" % sys.argv[0])
        return 0

    print("Running dotgit")

    cmd = sys.argv[1]
    if cmd == "init":
        return run_dotgit_init()
    elif cmd == "scatter":
        return run_dotgit_scatter()
    elif cmd == "gather":
        return run_dotgit_gather()

    print("Unknown command '%s'" % cmd)
    return 1
