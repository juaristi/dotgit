import os
from dotgit.classes import BaseDirectory, Executor, YamlReader, get_home_directory

def get_default_root_directory():
    rootdir = os.getenv("DOTGIT_ROOT_DIRECTORY")
    return rootdir if rootdir else get_home_directory()

def expand_if_needed(path):
    return os.path.expanduser(path) if path.startswith('~') else path

class Initializer(object):
    def run(self, url):
        base_dir = BaseDirectory()
        executor = Executor(base_dir.path)
        executor.e("git clone %s" % url)

class Scatterer(object):
    def run(self):
        with BaseDirectory() as basedir:
            y = YamlReader(os.path.join(basedir.path, 'dotgit.yml'))

            tracked_files = y.get('files', typ=list)

            rootdir = y.get('root_directory', typ=str)
            if not rootdir:
                rootdir = get_default_root_directory()

            basedir.path = rootdir
            print("Destination root directory: '%s'" % rootdir)

            for filename in tracked_files:
                dstpath, copied = basedir.copy2(expand_if_needed(filename))
                if copied:
                    print("Copied '%s' --> '%s'" % (filename, dstpath))
                else:
                    print("Skipping '%s' --> '%s'. Local file is more recent." % (filename, dstpath))

class Gatherer(object):
    def run(self):
        with BaseDirectory() as basedir:
            y = YamlReader(os.path.join(basedir.path, 'dotgit.yml'))

            tracked_files = y.get('files', typ=list)

            rootdir = y.get('root_directory', typ=str)
            if not rootdir:
                rootdir = get_default_root_directory()

            print("Root directory: %s" % rootdir)

            for filename in list(map(expand_if_needed, tracked_files)):
                fullpath = os.path.join(rootdir, filename) if not os.path.isabs(filename) else filename
                dstpath = basedir.copy(expand_if_needed(fullpath))
                if dstpath:
                    print("Copied '%s' --> '%s'" % (fullpath, dstpath))
                else:
                    print("File '%s' not found. Skipped." % fullpath)
