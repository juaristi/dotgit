import os
import shutil
import subprocess
import yaml

def get_home_directory():
    homedir = os.getenv("HOME", None)
    if homedir:
        return homedir

    homedir = os.path.expanduser("~")
    if homedir != "~":
        return homedir

    raise OSError("Could not look up user's home directory")

def find_git_repository(rootdir):
    for basename in os.listdir(rootdir):
        fullpath = os.path.join(rootdir, basename)
        if os.path.isdir(fullpath) \
                and ('.git' in os.listdir(fullpath)):
            return basename

def cmpmtime(p1, p2):
    """
    Compare mtimes of given two paths
    Return -1 if p1 is more recent than p2, 1 if p2 is more recent than p1
    """
    stp1, stp2 = os.stat(p1), os.stat(p2)
    if stp1.st_mtime > stp2.st_mtime:
        return -1
    elif stp1.st_mtime < stp2.st_mtime:
        return 1
    return 0

class YamlReader(object):
    def __init__(self, path):
        with open(path) as fp:
            self._y = yaml.load(fp.read(), Loader=yaml.SafeLoader)

    def get(self, key, default_value=None, typ=None):
        try:
            val = self._y[key]
        except KeyError:
            return default_value

        if typ:
            self._check(key, val, typ)

        return val

    def _check(self, key, value, typ):
        if type(value) != typ:
            raise ValueError("Invalid type for key '%s'. Type: '%s'; Expected: '%s'"
                             % (key, type(value), typ))

class BaseDirectory(object):
    def __init__(self):
        self._gitrepo = None
        self._prefix = get_home_directory()
        self._rootdir = 'dotgit'
        self.path = os.path.join(self._prefix, self._rootdir)
        self._initialize_base_directory_if_needed()
        self._rootdir = os.path.join('dotgit', self._gitrepo)
        self.path = os.path.join(self._prefix, self._rootdir)

    def copy(self, path):
        basename = os.path.basename(path)
        dstpath = os.path.join(self.path, basename)
        try:
            shutil.copyfile(path, dstpath)
            return dstpath
        except FileNotFoundError:
            return

    def copy2(self, path):
        basename = os.path.basename(path)
        if os.path.abspath(path):
            srcpath = \
                os.path.join(self._prefix,
                    os.path.join(self._rootdir, basename))

            can_copy = (cmpmtime(srcpath, path) <= 0)
            if can_copy:
                shutil.copyfile(srcpath, path)

            return path, can_copy
        else:
            dstpath = os.path.join(self.path, basename)

            can_copy = (cmpmtime(path, dstpath) <= 0)
            if can_copy:
                shutil.copyfile(path, dstpath)

            return dstpath, can_copy

    def __enter__(self):
        return self

    def __exit__(self, exc_type, value, traceback):
        pass

    def _initialize_base_directory_if_needed(self):
        try:
            if len(os.listdir(self.path)) > 0:
                print("Directory is not empty")
            self._gitrepo = find_git_repository(self.path)
            if self._gitrepo:
                print("Found git repository at '%s'" % self._gitrepo)
            else:
                print("No git repository found")
            print("Using base directory '%s'" % self.path)
        except FileNotFoundError:
            print("Directory '%s' does not exist. Creating." % self._rootdir)
            os.mkdir(self.path)
            print("Using base directory '%s'" % self.path)


class TemporaryBashrcFile(object):
    def __init__(self, prompt=None, dirname=".dotgit", workdir=None):
        self._prompt = prompt
        self._dirname = dirname
        self._workdir = workdir
        if self._prompt is None:
            self._prompt = '(dotgit gather) '

    def __enter__(self):
        if self._prompt is None:
            raise ValueError("Prompt was not set")

        return self._initialize_tmp_directory()

    def __exit__(self, exc_type, exc_value, traceback):
        shutil.rmtree(os.path.dirname(self._fullpath))

    def _initialize_tmp_directory(self, retry=True):
        directory = os.path.join(os.getcwd(), self._dirname)
        self._fullpath = os.path.join(directory, "dotgit_tmp_bashrc")

        try:
            os.chdir(directory)
        except FileNotFoundError:
            if not retry:
                raise OSError("Could not create temporary directory at: {}".format(directory))
            os.mkdir(directory)
            return self._initialize_tmp_directory(retry=False)
        except NotADirectoryError:
            raise NotADirectoryError("Could not create temporary directory at: {}. File already exists." \
                .format(directory))
        except PermissionError:
            raise PermissionError("Could not enter directory at: {}. Permission denied." \
                .format(directory))

        # Retrieve user's home directory, and create a temporary bashrc file
        home_directory = get_home_directory()
        bashrc_content = """source {}
PS1="{} \[\033[01;32m\][\\u@\h] \$\[\033[00m\] "
{}
""".format(os.path.join(home_directory, ".bashrc"), self._prompt, self._set_up_workdir())

        try:
            self._fp = open(self._fullpath, "w")
            self._fp.write(bashrc_content)
            return self._fp.name
        finally:
            self._fp.close()

    def _set_up_workdir(self):
        return self._workdir if self._workdir else ""

class Executor(object):
    def __init__(self, workdir=None):
        self.prompt = None
        self._workdir = workdir

    def execute(self, cmdline, shell='/bin/bash'):
        cmd = list(map(lambda e: e.strip(), cmdline.split(' ')))
        subprocess.run(cmd, cwd=self._workdir)

    e = execute
