#!/usr/bin/env python3
import os
import shutil
import subprocess
from tempfile import mkstemp

class TemporaryBashrcFile(object):
    def __init__(self, prompt='(dotgit gather) ', dirname=".dotgit"):
        self._prompt = prompt
        self._dirname = dirname

    def __enter__(self):
        if self._prompt is None:
            raise ValueError("Prompt was not set")

        return self._initialize_tmp_directory()

    def __exit__(self, exc_type, exc_value, traceback):
        shutil.rmtree(os.path.dirname(self._fullpath))

    def _initialize_tmp_directory(self, retry=True):
        directory = os.path.join(os.getcwd(), self._dirname)
        self._fullpath = os.path.join(directory, "dotgit_tmp_bashrc")

        try:
            os.chdir(directory)
        except FileNotFoundError:
            if not retry:
                raise OSError("Could not create temporary directory at: {}".format(directory))
            os.mkdir(directory)
            return self._initialize_tmp_directory(retry=False)
        except NotADirectoryError:
            raise NotADirectoryError("Could not create temporary directory at: {}. File already exists." \
                .format(directory))
        except PermissionError:
            raise PermissionError("Could not enter directory at: {}. Permission denied." \
                .format(directory))

        # Retrieve user's home directory, and create a temporary bashrc file
        home_directory = self._get_home_directory()
        bashrc_content = """source {}
PS1="{} \[\033[01;32m\][\\u@\h] \$\[\033[00m\] "
{}
""".format(os.path.join(home_directory, ".bashrc"), self._prompt, self._set_up_git_environment())

        try:
            self._fp = open(self._fullpath, "w")
            self._fp.write(bashrc_content)
            return self._fp.name
        finally:
            self._fp.close()

    def _get_home_directory(self):
        homedir = os.getenv("HOME", None)
        if homedir:
            return homedir

        homedir = os.path.expanduser("~")
        if homedir != "~":
            return homedir

        raise OSError("Could not look up user's home directory")

    def _set_up_git_environment(self):
        return "git init"

class Executor(object):
    def __init__(self):
        self.prompt = None

    def execute(self, shell='/bin/bash'):
        with TemporaryBashrcFile(prompt=self.prompt) as filename:
            subprocess.run([shell, "--rcfile", filename])

class Gatherer(object):
    def gather(self):
        executor = Executor()
        executor.prompt = "(dotgit gather)"
        executor.execute()

if __name__ == '__main__':
    Gatherer().gather()

