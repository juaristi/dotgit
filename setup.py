import os

__pkginfo__ = {}
base_dir = os.path.dirname(__file__)

with open(os.path.join(base_dir, "dotgit", "__pkginfo__.py")) as fp:
    exec(fp.read(), __pkginfo__)

return setup(
    name="dotgit",
    version=__pkginfo__["version"],
    license=__pkginfo__["license"],
    description=__pkginfo__["description"],
    author=__pkginfo__["author"],
    url=__pkginfo__["url"],
    python_requires=">=3.6",
    entry_points={
        "console_scripts": [ "dotgit = dotgit:run_dotgit" ]
    }
)

